import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mbutakov on 025 25.04.17.
 */
public class Parser {
    public List<Mail> parse(String path) throws Exception {
        List<Mail> result=new ArrayList<Mail>();
        List<String> strings = Files.readAllLines(Paths.get(path));
        Date currentDate= null;
        DateFormat simpleDateFormat=new SimpleDateFormat("MM dd yyyy");
        String currentStr ="";
        Mail mail=null;
        for(int i=0, len=strings.size(); i<len; i++){
            currentStr =strings.get(i).trim();

            if(currentStr.isEmpty()){
                //System.out.println(String.format("Строка {%d} пустая",i));
                continue;
            }
            if(currentStr.matches("\\w+ \\d+ \\d+")){
                currentDate=simpleDateFormat.parse(currentStr);
                //System.out.println(String.format("Строка {%d} - дата {%s}",i,currentDate));
                continue;
            }

            if(currentStr.startsWith("phone")){
                //System.out.println(String.format("Строка {%d} - письмо, начало",i));
                if(mail!=null)result.add(mail);//предыдущее письма
                mail=new Mail(currentDate,currentStr);
            } else {
                //System.out.println(String.format("Строка {%d} - письмо, продолжение",i));
                mail.setContent(mail.getContent()+"\n"+currentStr);
            }
        }
        if(mail!=null)result.add(mail);
        return result;
    }
}
