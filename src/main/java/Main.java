import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;

/**
 * Created by mbutakov on 025 25.04.17.
 */
public class Main {
    public static void main(String[] argv){
        if(argv.length==0)return;
        String filepath= argv[0];
        System.out.println("Передан файл "+filepath);
        Properties property = new Properties();
        int mindelay=0;
        int maxdelay=0;
        boolean reallySending =false;

        try {
            FileInputStream confReader = new FileInputStream("src/main/resources/config.properties");
            property.load(confReader);
            maxdelay=Integer.parseInt(property.getProperty("mail.maxdelay","100"));
            mindelay=Integer.parseInt(property.getProperty("mail.mindelay","5000"));
            reallySending=!property.getProperty("mail.realMode","").equals("");
            property.setProperty("mail.title",
                    new String(property.getProperty("mail.title").getBytes("CP1252"),
                            "CP1251"));

            System.out.print("Введите пароль для пользователя "+property.getProperty("mail.username")+"@gmail.com: ");
            BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
            property.setProperty("mail.username", consoleReader.readLine());
            for(int i=0, len=property.getProperty("mail.username").length(); i<len; i++)
                System.out.println("\r");
        }catch (Exception e){
            System.out.print("Ой, чтение конфигов упало " + e.toString());
            return;
        }

        Parser parser=new Parser();
        List<Mail> mails = null;
        try {
            mails= parser.parse(filepath);
            mails.forEach(mail -> System.out.println(mail.pretty()+"\n"));
        }catch(Exception e){
            System.out.print("Ой, парсинг упал " + e);
            return;
        }

        try {
            Sender sender=new Sender();
            System.out.println("Настоящая отправка - "+reallySending);
            for (int i=0, len=mails.size(); i<len; i++) {
                int delay=(int)(Math.random() * maxdelay) + mindelay;
                if(reallySending) {
                    sender.Send(
                            property.getProperty("mail.username"),
                            property.getProperty("mail.password"),
                            property.getProperty("mail.senderEmail"),
                            property.getProperty("mail.recipientEmail"),
                            property.getProperty("mail.title"),
                            mails.get(i).pretty()
                    );
                }
                System.out.println("Отправлено письмо "+i+", пауза "+delay+"мс");
                Thread.sleep(delay);
            }
        }catch(Exception e){
            System.out.print("Ой, отправка упала " + e);
            return;
        }
    }
}
