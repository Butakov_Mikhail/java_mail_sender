import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mbutakov on 025 25.04.17.
 */
public class Mail {
    public Mail(Date date, String content) {
        this.date = date;
        this.content = content;
    }

    Date date;
    String content;

    public Date getDate() {return date;}
    public void setDate(Date date) {
        this.date = date;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String pretty(){
        String pretty=content
                .replace("phone","phone:")
                .replace(", email","\nemail:")
                .replace(", text","\ntext")
                .replace(", address","\naddress:")
                ;
        return "date: "+date+"\n"+pretty;
    }
}
